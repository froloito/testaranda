﻿using System;

namespace PruebaArandaAlexanderPaz.ModelDTO
{
    public class ProductoDTO
    {
        public long Id { get; set; }
        public int CategoriaID { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string ImagenUrl { get; set; }
        public DateTime FechaCreacion { get; set; }

    }
}
