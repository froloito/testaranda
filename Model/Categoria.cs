﻿using System;

namespace PruebaArandaAlexanderPaz.Model
{
    public class Categoria
    {
        public long Id { get; set; }
        public int Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }

    }
}
