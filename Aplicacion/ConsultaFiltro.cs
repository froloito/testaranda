﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PruebaArandaAlexanderPaz.Model;
using PruebaArandaAlexanderPaz.ModelDTO;
using PruebaArandaAlexanderPaz.Persistencia;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PruebaArandaAlexanderPaz.Aplicacion
{
    public class ConsultaFiltro
    {
        public class ProductoUnico : IRequest<ProductoDTO>
        {
            public int productoId { get; set; }
        }
        public class Manejador : IRequestHandler<ProductoUnico, ProductoDTO>
        {
            private readonly ContextDB _contextDB;
            private readonly IMapper _mapper;

            public Manejador(ContextDB contextDB, IMapper mapper)
            {
                _contextDB = contextDB;
                _mapper = mapper;
            }

            public async Task<ProductoDTO> Handle(ProductoUnico request, CancellationToken cancellationToken)
            {
                var producto = await _contextDB.Producto.Where(f => f.Id == request.productoId).FirstOrDefaultAsync();
                if(producto == null)
                {
                    throw new Exception("No se encontró el producto");
                }
                var productoDto = _mapper.Map<Producto, ProductoDTO>(producto);
                return productoDto;
            }
        }
    }
}
