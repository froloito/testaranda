﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PruebaArandaAlexanderPaz.Model;
using PruebaArandaAlexanderPaz.ModelDTO;
using PruebaArandaAlexanderPaz.Persistencia;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PruebaArandaAlexanderPaz.Aplicacion
{
    public class Consulta
    {
        public class Ejecuta : IRequest<List<ProductoDTO>>
        {            
        }
        public class Manejador : IRequestHandler<Ejecuta, List<ProductoDTO>>
        {
            private readonly ContextDB _contexto;
            private readonly IMapper _mapper;

            public Manejador(ContextDB contexto, IMapper mapper)
            {
                _contexto = contexto;
                _mapper = mapper;
            }

            public async Task<List<ProductoDTO>> Handle(Ejecuta request, CancellationToken cancellationToken)
            {
                var productos = await _contexto.Producto.ToListAsync();               
                //pasamos del tipo productos al tipo productosdto _mapper.map<inputorigen,outputdestino>(objecto que se mapeará)
                var productosDto=_mapper.Map<List<Producto>,List<ProductoDTO>>(productos);
                return productosDto;

            }
        }
    }
}
