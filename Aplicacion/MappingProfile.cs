﻿using AutoMapper;
using PruebaArandaAlexanderPaz.Model;
using PruebaArandaAlexanderPaz.ModelDTO;

namespace PruebaArandaAlexanderPaz.Aplicacion
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Producto, ProductoDTO>();
        }
    }
}
