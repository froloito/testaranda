﻿using FluentValidation;
using MediatR;
using PruebaArandaAlexanderPaz.Model;
using PruebaArandaAlexanderPaz.Persistencia;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PruebaArandaAlexanderPaz.Aplicacion
{
    public class Nuevo
    {
        public class Ejecuta : IRequest
        {
            public long? Id { get; set; }
            public string Nombre { get; set; }
            public string Descripcion { get; set; }
            public string ImagenUrl { get; set; }
            public int CategoriaId { get; set; }
            public DateTime FechaCreacion { get; set; }

        }
        public class EjcutaValidador : AbstractValidator<Ejecuta> {
            public EjcutaValidador() {
                RuleFor(x => x.Nombre).NotEmpty();
                RuleFor(x => x.Descripcion).NotEmpty();
                RuleFor(x => x.CategoriaId).NotEmpty();
                RuleFor(x => x.FechaCreacion).NotEmpty();
                RuleFor(x => x.ImagenUrl).NotEmpty();
            }

        }
        public class Manejador : IRequestHandler<Ejecuta>
        {
            private readonly ContextDB _Context;

            public Manejador(ContextDB context)
            {
                _Context = context;
            }

            public async Task<Unit> Handle(Ejecuta request, CancellationToken cancellationToken)
            {
                if(request.Id == null)
                {
                    var producto = new Producto
                    {
                        Nombre = request.Nombre,
                        Descripcion = request.Descripcion,
                        CategoriaID = request.CategoriaId,
                        FechaCreacion = request.FechaCreacion,
                        ImagenUrl=request.ImagenUrl
                    };
                    _Context.Producto.Add(producto);
                    var value = await _Context.SaveChangesAsync();
                    if (value > 0)
                    {
                        return Unit.Value;
                    }
                    throw new Exception("No se pudo guardar el producto");
                }
                else
                {
                    var producto = new Producto
                    {
                        Id= (long)request.Id,
                        Nombre = request.Nombre,
                        Descripcion = request.Descripcion,
                        CategoriaID = request.CategoriaId,
                        FechaCreacion = request.FechaCreacion,
                        ImagenUrl=request.ImagenUrl
                    };
                    _Context.Producto.Update(producto);
                    var value = await _Context.SaveChangesAsync();
                    if (value > 0)
                    {
                        return Unit.Value;
                    }
                    throw new Exception("No se pudo actualizar el producto");
                }
                
               


            }
        }

    }
}
