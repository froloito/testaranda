﻿using Microsoft.EntityFrameworkCore;
using PruebaArandaAlexanderPaz.Model;

namespace PruebaArandaAlexanderPaz.Persistencia
{
    public class ContextDB : DbContext
    {
        public ContextDB(DbContextOptions<ContextDB> options) : base(options) { }
        public DbSet<Producto> Producto { get; set; }
        public DbSet<Categoria> Categoria { get; set; }


    }
}
