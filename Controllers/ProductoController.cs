﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using PruebaArandaAlexanderPaz.Aplicacion;
using PruebaArandaAlexanderPaz.ModelDTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaArandaAlexanderPaz.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ProductoController(IMediator mediator)
        {
            _mediator=mediator;
        }
        [HttpPost]
        public async Task<ActionResult<Unit>> Crear(Nuevo.Ejecuta data)
        {
            return await _mediator.Send(data);
        }
        [HttpGet]
        public async Task<ActionResult<object>> productos(string nombre,string descripcion,int? categoria, int limit, int pagina, bool paginar = false)
        {
            var ListProductos= await _mediator.Send(new Consulta.Ejecuta());
            if (nombre != null)
            {
                ListProductos = ListProductos.Where(f=>f.Nombre.Contains(nombre)).ToList();
            }
            if (descripcion != null)
            {
                ListProductos = ListProductos.Where(f => f.Descripcion.Contains(descripcion)).ToList();
            }
            if (categoria != null)
            {
                ListProductos = ListProductos.Where(f => f.CategoriaID== categoria).ToList();
            }
            if (!paginar)
            {
                return ListProductos;
            }
            if (limit == 0)
                return new
                {
                    totalRegistro = ListProductos.Count(),
                    current = 0,
                    page = 0,
                    data = ListProductos.ToList()
                };
            else
                return new
                {
                    totalRegistro = ListProductos.Count(),
                    current = pagina + 1,
                    page = int.Parse((ListProductos.Count() / limit).ToString()) + 1,
                    data = ListProductos.Skip(pagina * limit).Take(limit).ToList()
                };
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductoDTO>> producto(int id)
        {
            return await _mediator.Send(new ConsultaFiltro.ProductoUnico { productoId=id});
        }
        [HttpPut()]
        public async Task<ActionResult<Unit>> Actualizar(Nuevo.Ejecuta data)
        {
            return await _mediator.Send(data);
        }

    }
}
